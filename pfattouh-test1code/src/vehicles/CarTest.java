package vehicles;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTest {

	@Test
	void test() {
		
		int testSpeed=15;
		
		Car tester=new Car(testSpeed);
		assertEquals(tester.getSpeed(),testSpeed);
		assertEquals(tester.getLocation(),0);
		
		tester.accelerate();
		assertEquals(tester.getSpeed(),testSpeed+1);
		
		tester.decelerate();
		assertEquals(tester.getSpeed(),testSpeed);	
		
		tester.decelerate();
		assertEquals(tester.getSpeed(),testSpeed-1);
		
		tester.moveLeft();
		assertEquals(tester.getLocation(),0-(testSpeed-1));
		
		tester.moveRight();
		assertEquals(tester.getLocation(),0);
		
		
		
		Car testerZeroSpeed= new Car(0);
		tester.decelerate();
		assertEquals(testerZeroSpeed.getSpeed(),0);	
		
		//fail("Not yet implemented");
	}

}
