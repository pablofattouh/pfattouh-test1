package utilities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MatrixMethodTest {

	@Test
	public void testDuplicate() {
		MatrixMethod tester= new MatrixMethod();
		int[][] result = tester.duplicate({{1,2,3},{4,5,6}});
		assertEquals("{{1,2,3,1,2,3},{4,5,6,4,5,6}}", tester.duplicate({{1,2,3},{4,5,6}}));
		fail("Not yet implemented");
	}

}
